#include <iostream>
#include <ctime>
#define arraySize 10

using namespace std;

int main() 
{
    int a[arraySize];
    int i,j,temp;
    // seed random number generation to unix time
    srand(time(NULL));

    // initialize a[] for testing
    for (i=0; i<arraySize;i++)
    {
        a[i] = i;
    }

    //fisher and yates modern algorithm
    for (i = arraySize-1; i > 0; i--)
    {
        // Set placeholder to a random variable
        // Modulo allows range to be given to rand(), which takes
            // no parameters
        j = (rand() % arraySize) + 1;
        cout << j << endl;
        temp = a[j]; 
        a[j] = a[i];
        a[i] = temp;
    }
    // test output
    // for (i=0; i<arraySize-1;i++)
    // {
    //     cout << a[i] << endl;
    // }
    return 0;
}