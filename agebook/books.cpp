/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: Age Appropriate Books    *
 * Due Date: October 7th 2019           *
 * Last Modified: *
 ****************************************/

#include <iostream>
#include <fstream>
#include <string.h>
#include <cctype>
#include "singlelist.h"


using namespace std;

void sanitizeInput(char input[])
{
    int i;
    for (i = 0; i < LONGESTWORD; i++) 
    {
        input[i] = tolower(input[i]);
        if (!isalpha(input[i]))
            input[i] = '\0';
    }
}
int main() 
{
    singleLinkedList commonnames, commonwords;
    singleLinkedList inCommonWords, inOtherWords;
    char inputfile[LONGESTWORD];
    char inputword[LONGESTWORD];
    ifstream infile;

    // Put commonnames into commonnames list
    infile.open("/colossus/classes/cs201/words/commonNames");
    while (infile >> inputword)
      commonnames.insert(inputword);
    infile.close();
    // Clear filestream to reuse the variable
    infile.clear();

    // Put commonWords into commonwords list
    infile.open("/colossus/classes/cs201/words/commonWords");
    while (infile >> inputword)
      commonwords.insert(inputword);
    infile.close();
    // Clear filestream to reuse the variable
    infile.clear();

    cout << "Please enter your input file name: ";
    cin >> inputfile; 
    infile.open(inputfile);
    while (infile >> inputword)
    {
        sanitizeInput(inputword);
        if (commonwords.find(inputword))
            inCommonWords.insert(inputword);
        else
            inOtherWords.insert(inputword);
    }
    cout << "Common:\n";
    inCommonWords.print();
    printf("Length: %d\n\n",inCommonWords.length());
    cout << "Uncommon:\n";
    inOtherWords.print();
    printf("Length: %d\n\n",inOtherWords.length());
    printf("File '%s' has:\n",inputfile);
    printf("%.2f%% Common Words, %.2f%% Uncommon Words\n",
        (float)inCommonWords.length()*100/(float)(inCommonWords.length()+inOtherWords.length())
        ,(float)inOtherWords.length()*100/(float)(inCommonWords.length()+inOtherWords.length()));

    infile.close();
    

    return 0;
}
