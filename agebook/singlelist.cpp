#include "singlelist.h"
#include <string.h>
#include <iostream>

using namespace std;

singleLinkedList::singleLinkedList()
{
    head = NULL;
}
singleLinkedList::~singleLinkedList()
{
    node * curr;
    node * temp;
    
    curr = head;

    while (curr != NULL)
    {
        temp = curr;
        curr = curr -> next;
        delete(temp);
    }
    head = NULL;
}
void singleLinkedList::print()
{
    node * curr = head;
    while (curr != NULL)
    {
        printf("\t%s\n\t%i Occurances\n\n",curr -> word, (curr -> duplicateCount+1));
        curr = curr -> next;
    }
}
bool singleLinkedList::find(char input[])
{
    node * curr;

    curr = head;

    while (curr -> next != NULL && strcmp(curr -> next -> word,input) != 0)
        curr = curr -> next;
    if (curr -> next == NULL)
        return false;
    return true;
}
bool singleLinkedList::insert(char newWord[])
{
    node * insert = head;
    node * trail = NULL;
    
    if (insert == NULL)
    {
		// Empty case
		head = new(node);
        strncpy(head -> word, newWord,LONGESTWORD);
		head -> next = NULL;
	}
	else
	{
		// Search for our insertion spot
		while (insert != NULL && strcmp(newWord,insert -> word) > 0)
		{
			trail = insert;
			insert = insert -> next;
		}

		if (insert != NULL && strcmp(insert -> word, newWord) == 0)
		{
			// Duplicate ID
            insert -> duplicateCount++;
			return false;
		}
		else if (trail == NULL)
		{
			// Insert before the head
			insert = new(node);
            strncpy(insert -> word, newWord,LONGESTWORD);
			insert -> next = head;
			head = insert;
		}
		else if (insert == NULL)
		{
			// Insert after the end
			trail -> next = new(node);
			insert = trail -> next;
            strncpy(insert -> word, newWord,LONGESTWORD);
			insert -> next = NULL;
        }
		else
		{
            // General case for insertion
            insert = new(node);
            strncpy(insert -> word, newWord,LONGESTWORD);
            insert -> next = trail -> next;
            trail -> next = insert;
	    }
    }
    return true;
}
int singleLinkedList::length()
{
    int lengthvar = 0;
    node * curr = head;

    while (curr != NULL && curr -> next != NULL)
    {
        lengthvar += 1;
        curr = curr -> next;
    }
    return lengthvar;
}