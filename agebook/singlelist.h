#define LONGESTWORD 30

struct node
{
    char word[LONGESTWORD];
    int duplicateCount;
    node * next;
};

class singleLinkedList
{
    private:
        node * head;
    public:
        singleLinkedList();
        ~singleLinkedList();
        bool insert(char newWord[]);
        bool find(char theWord[]);
        void print();
        int length();
};