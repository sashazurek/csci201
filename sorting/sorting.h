#include <iostream>

class Item
{
    public:
        Item();
        std::string name;
        int amount;
        int weight;
        int totalWeight;
        int value;
        int totalValue;
};

Item::Item()
{
    name.clear();
    amount = 0;
    weight = 0;
    int totalWeight = 0;
    value = 0;
    int totalValue = 0;
}