/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: Sorting                  *
 * Due Date: December 5th, 2019         *
 * Last Modified: December 4th, 2019    *
 ****************************************/
#define EBAYITEMMAX 15
#define CLITEMMAX 20
#define SEAMOREMAX 5
#define ASCIILEN 128
#include <algorithm>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include "sorting.h"

using namespace std;

/***********************************************************
 * Function: radixSort()
 * Author: Michael Zurek 
 * Purpose: To sort a series of strings
 * Parameters: Vector of items, string array
 * Return Value: None
 **********************************************************/
void radixSort(vector<Item> &items, string arr[])
{
    int maxStrLen = 0,i,x,y,z;
    int bucketindex,arrindex;
    int curr[ASCIILEN];
    string ** buckets;

    // find and set maxStrLen
    for (i = 0; i < items.size(); i++)
    {
        if (maxStrLen < items[i].name.length())
            maxStrLen = items[i].name.length();
    }

    // copy item names to arr
    for (i = 0; i < items.size(); i++)
    {
        arr[i] = items[i].name;
    }

    for (i = maxStrLen-1; i >= 0; i--)
    {
        // initialize the buckets
            // 1: initialize the buckets for each ascii
        buckets = new string * [ASCIILEN];
        for (x = 0; x < ASCIILEN; x++)
        {
            // 2: initialize enough spots for strings in a bucket
            curr[x] = 0;
            buckets[x] = new string [items.size()];
        }

        for (z = 0; z < items.size(); z++)
        {
            bucketindex = items[z].name[i];
            buckets[bucketindex][curr[bucketindex]] = items[z].name;
            curr[bucketindex] += 1;
        }

        // Empty buckets into string array

        arrindex = 0;
        for (x = 0; x < ASCIILEN; x++)
        {
            for (y = 0; y < curr[x]; y++)
            {
                arr[arrindex] = buckets[x][y];
                arrindex++;
            }
        }
    }
}
/***********************************************************
 * Function: qsPartition()
 * Author: Michael Zurek 
 * Purpose: Helper function for quicksort
 * Parameters: Vector of items, low pos, high pos
 * Return Value: int
 **********************************************************/
int qsPartition(vector<Item> &items,int low, int high)
{
    int pivotweight = items[high].totalWeight;
    int i = low;
    int j;
    Item temp;

    for (j = low; j < high; j++)
    {
        if (items[j].totalWeight < pivotweight)
        {
            temp = items[j];
            items[j] = items[i];
            items[i] = temp;
            i++;
        }
    }
    temp = items[i];
    items[i] = items[high];
    items[high] = temp;
    return i;
}
/***********************************************************
 * Function: quicksort()
 * Author: Michael Zurek 
 * Purpose: To sort a series of items based on Quicksort algorithm
 * Parameters: Vector of items, low pos, high pos
 * Return Value: None
 **********************************************************/
void quicksort(vector<Item> &items,int low,int high)
{
    if (low < high)
    {
        int p = qsPartition(items,low,high);
        quicksort(items,low,p-1);
        quicksort(items,p+1,high);
    }
}
/***********************************************************
 * Function: bubbleSort()
 * Author: Michael Zurek 
 * Purpose: To sort a series of numbers in order
 * Parameters: vector of items
 * Return Value: None
 **********************************************************/
void bubbleSort(vector<Item> &items)
{
    bool swapped;
    int i,n = items.size();
    Item tempItem;

    // Slightly modified bubble sort that can break early
    // Sorts values of items greatest to least
    do
    {
        swapped = false;
        for (i = 1; i < n; i++)
        {
            if (items[i-1].totalValue < items[i].totalValue)
            {
                tempItem = items[i-1];
                items[i-1] = items[i];
                items[i] = tempItem;
                swapped = true;
            }
        }
        n--;
    } while (swapped);
}

int main() 
{
    ifstream input("/colossus/classes/cs201/bunnies.stuff"); // CHANGE ME WHEN TURNING IN
    int i;
    Item tempItem;
    string line,temp;
    string * radix;
    vector<Item> items;
    

    // take in items and store them in vector ITEMS
    // sample input: $(name):$(amount):$(weight):$(value)
    while(getline(input,line))
    {
        // Set name of item
        for (i = 0; line[i] != ':'; i++)
        {
            tempItem.name += line[i];
        }

        // Set amount of item
        for (i += 1; line[i] != ':'; i++)
        {
            temp += line[i];
        }
        tempItem.amount = atoi(temp.c_str());
        temp.clear();

        // Set weight of item
        for (i += 1; line[i] != ':'; i++)
        {
            temp += line[i];
        }
        tempItem.weight = atoi(temp.c_str());
        temp.clear();       

        // Using a cstring instead of a c++ string
            // for the last for loop fixes a weird segfault.
            // Schemm, I really, truly, am sorry.
        char * cstr = new char [line.length()+1];
        strcpy(cstr,line.c_str());

        // Set value of item
        for (i += 1; cstr[i] != ':'; i++)
        {
            temp += line[i];
        }
        tempItem.value = atoi(temp.c_str());
        temp.clear();
        delete(cstr); // No wasting memory here!

        // Make total of value for Ebay
        tempItem.totalValue = tempItem.value * tempItem.amount;

        // Make total of weight for Craigslist
        tempItem.totalWeight = tempItem.weight * tempItem.amount;

        // Push tempItem to the vector
        items.push_back(tempItem);
        // Reset temp variables
        tempItem.name.clear();
        line.clear();
    }
    // Bubble sort the vector for Ebay
    bubbleSort(items);
    // Print out top 15 most valuable items from vector
        // Can print first 15 items on list as bubbleSort sorts High to low
    printf("Top 15 Most Valuable Items for Ebay\n");
    for (i = 0; i < EBAYITEMMAX; i++)
    {  
        printf("\t%i: %s\n",i+1,items[i].name.c_str());
    }

    cout << endl << endl;

    quicksort(items,0,items.size()-1);

    // Print out the top 20 heaviest items from vector
    printf("Top 20 Heaviest Items for Craigslist\n");
    for (i = CLITEMMAX; i > 0; i--)
    {  
        printf("\t%i: %s (%i)\n",i,items[items.size() - i].name.c_str(),
            items[items.size() - i].totalWeight);
    }

    radix = new string[items.size()];

    radixSort(items,radix);
    printf("\nFirst 5 Items Alphabetically for Seamore's Closet\n");
    for (i = 0; i < SEAMOREMAX; i++)
        printf("\t%i: %s\n",i+1,radix[i].c_str());
    return 0;
}
