/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: Hi-Low  Solitaire        *
 * Due Date: September 12th, 2019       *
 * Last Modified: September 8nd, 16:04  *
 ****************************************/

#include <iostream>
#include <algorithm>
#include <ctime>
#include "stack.h"
#define deckSize 52
#define startingCash 500
#define pileCount 4
#define gamePrice 150

using namespace std;

/***********************************************************
 * Function: printUserStatus()
 * Author: Michael Zurek 
 * Purpose: To display user status; money
 * Parameters: cashOnHand
 * Return Value: Nothing 
 * Last Modified: August 27th, 21:17
 **********************************************************/
void printUserStatus(int cash)
{
    cout << "Money: $" << cash << endl;
}
/***********************************************************
 * Function: printUserStatus()
 * Author: Michael Zurek 
 * Purpose: To display user status; money
 * Parameters: cashOnHand
 * Return Value: Nothing 
 * Last Modified: August 27th, 21:17
 **********************************************************/
void dealDeck(card deck[deckSize], Stack piles[pileCount])
{
    int i;

    for (i = 0; i < 13; i++) 
        piles[0].push(deck[i]);
    for (i = 13; i < 26; i++)
        piles[1].push(deck[i]);
    for (i = 26; i < 39; i++)
        piles[2].push(deck[i]);
    for (i = 39; i < 52; i++)
        piles[3].push(deck[i]);
}

/***********************************************************
 * Function: initializeDeck()
 * Author: Michael Zurek 
 * Purpose: To initialize and shuffle the deck of cards
 * Parameters: Integer array
 * Return Value: Nothing (function modifies theDeck in main)
 * Last Modified: September 1st, 23:39
 **********************************************************/
void initializeDeck(card deck[deckSize])
{
    int i;
    // initialize the deck    
	// 1 = Ace, 11 = Jack, 12 = Queen, 13 = King
	// 1 = Spades, 2 = Clubs, 3 = Hearts, 4 = Diamonds
    for (i=0; i < 13; i++)
    {
        deck[i].value = i+1;
        deck[i].face = 1;
    }
    for (i=13; i < 26; i++)
    {
        deck[i].value = i-12;
        deck[i].face = 2;
    }
    for (i=26; i < 39; i++)
    {
        deck[i].value = i-25;
        deck[i].face = 3;
    }
    for (i=39; i < 52; i++)
    {
        deck[i].value = i-38;
        deck[i].face = 4;
    }
    // shuffle of pre-initialized array
    // random shuffle needs pointers, hence the dereference
    // last parameter is exclusive, so max - 1 is not needed
    random_shuffle(&deck[0],&deck[deckSize]);
}
/***********************************************************
 * Function: destroyDeck()
 * Author: Michael Zurek 
 * Purpose: To initialize and shuffle the deck of cards
 * Parameters: Array of stacks
 * Return Value: Nothing (function modifies class)
 * Last Modified: August 27th, 21:17
 **********************************************************/
void destroyDeck(Stack deck[pileCount])
{
    int i;

    // Go through each pile and destroy them
    for (i=0; i < 4; i++)
        deck[i].~Stack();
}
/***********************************************************
 * Function: inputQuery()
 * Author: Michael Zurek 
 * Purpose: To ask user for input and sanitize it
 * Parameters: None
 * Return Value: char for user input
 * Last Modified: September 1st, 19:49
 **********************************************************/

char inputQuery()
{
    char input;
    bool valid = false;

    do
    {
        cout << "(H)igher, (l)ower, (s)hift, or (q)uit?" << endl;
        cin >> input;
    
        switch (input)
        {
            case 'h':
            case 'l':
            case 's':
            case 'c':
            case 'q':
                valid = true;
                break;
            default:
                cout << "Invalid input!\n";
                break;
        }
    } while (!valid);
    return input;
}
/***********************************************************
 * Function: verifyGuess()
 * Author: Michael Zurek 
 * Purpose: To verify if user guessed correctly
 * Parameters: input choice, flipped card, and the pile
 * Return Value: int for condition status
 * Last Modified: September 2nd, 17:23
 **********************************************************/

bool verifyGuess(char input, card flipped,Stack &pile) 
{
    //& is necessary in stack declaration to prevent double free()
    if (input == 'h')
    {
        if (flipped.value >= pile.getTop().value)
            return false;
    }
    else
    {
        if (flipped.value <= pile.getTop().value)
            return false;
    }
    return true;
}

/***********************************************************
 * Function: theGame()
 * Author: Michael Zurek 
 * Purpose: To play one round of hi-low Solitare
 * Parameters: cashOnHand from main()
 * Return Value: int for condition status
 * Last Modified: September 1st, 19:49
 **********************************************************/
int theGame(int &cashOnHand)
{
    int condition = 0;
    card theDeck[deckSize];
    int cheat = 1;
    card hold;
    int pile = 0;
    char userInput;
    Stack thePiles[pileCount];

    printUserStatus(cashOnHand);
    initializeDeck(theDeck);
    // Sort deck into thePiles
    dealDeck(theDeck, thePiles);
    // Allow user to continue guessing until either:
    //     User guesses wrong
    //     User wants to quit
    // Both result in the end of the round.
    do
    {
        // Check for cheat flag
        // If unset, operate normally
        // If set, unset flag and skip to inputQuery()
        if (cheat == 1)
        {
            //Print value of the top of the pile 
            // Check if pile is empty
                // If so, force pile change and add 500
            hold = thePiles[pile].pop();
            if (thePiles[pile].isEmpty())
            {
                cout << endl << endl;
                cout << "Wow! You guessed a whole pile correctly!\n";
                cout << "+$500 to your wallet\n";
                cout << endl << endl;
                cashOnHand += 500;
                printUserStatus(cashOnHand);
                pile++;
                hold = thePiles[pile].pop();
            }
            cout << "Current Pile: " << pile+1 << endl;
            cout << "Card: "; 
            cout << hold;
        } else
            cheat = 1;
        // Ask user for input
        userInput = inputQuery();
        // If input is h or l, verify guess
        if (userInput == 'h' || userInput == 'l')
        {
            // if guess is correct, add 10 dollars
                // if end of pile, add 500
            // else, set condition to 1 
            if (verifyGuess(userInput, hold,thePiles[pile]))
            {
                cout << "Good guess! +$10\n";
                cashOnHand += 10;
            }
            else
            {
                cout << "Bad guess! You lose :(\n";
                condition = 1;
            }
        }
        // If input is s, shift to next pile
        // Set game-end condition when pile 3 is activated
        else if (userInput == 's')
        {
            if (pile != 3)
                pile++;
            else
                condition = 2;
        }
        // If input is c, reveal unflipped card
        else if (userInput == 'c')
        {
            cout << thePiles[pile].getTop();
            // Set cheat flag, which skips popping 
            cheat = 0;
        }
        // If input is q, set condition to 1
        else
            condition = 1;
        if (condition == 0)
            printUserStatus(cashOnHand);
    } while (condition == 0);
    destroyDeck(thePiles);
    cout << endl << endl << endl;
    return condition;
}

int main() 
{
    int cashOnHand = startingCash;
    int status = 0;
    srand(time(NULL));

    // Game is contained in theGame()
    // theGame() returns a value 0-1
    // The values are assigned these conditions:
        // 0 - User would like to continue playing
            // Result: New deal
        // 1 - User would like to quit
            // Program ends

    do
    {
        // Verify user has enough cash to play
        if (cashOnHand >= gamePrice)
        {   
            cout << "Starting game. -$" << gamePrice << endl;
            cashOnHand -= gamePrice;
            status = theGame(cashOnHand);
        }
        else
        {
            cout << "Sorry, you ran out of money!" << endl;
            status = 1;
        }
        if (status == 1)
            cout << "Goodbye! Thanks for playing." << endl;
    } while (status != 1);
    return 0;
}