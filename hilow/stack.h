/******************************************
 * Name: Michael Zurek                     *
 * Class: CSCI 201, Fall 2019              *
 * Assignment: Hi-Low Solitaire            *
 * Due Date: September 12th, 2019          *
 * Last Modified: September 2nd, 2019      *
 ******************************************/

#include <iostream>
#include <fstream>

using namespace std;

struct card
{
	// 1 = Ace, 11 = Jack, 12 = Queen, 13 = King
	int value;
	// 1 = Spades, 2 = Clubs, 3 = Hearts, 4 = Diamonds
	int face;
};

inline ostream& operator <<(ostream& outs, const card theCard)
{
	if (theCard.value > 1 && theCard.value < 11)
		cout << theCard.value << " of ";
	else if (theCard.value == 1)
		cout << "Ace of ";
	else if (theCard.value == 11)
		cout << "Jack of ";
	else if (theCard.value == 12)
		cout << "Queen of ";
	else if (theCard.value == 13)
		cout << "King of ";
	else
		cout << "Invalid of ";
	if (theCard.face == 1)
		cout << "Spades";
	else if (theCard.face == 2)
		cout << "Clubs";
	else if (theCard.face == 3)
		cout << "Hearts";
	else
		cout << "Diamonds";
	cout << endl;	
	return outs;
}

/***********************
 * Class Name: Stack
 * Purpose: Establish a stack data structure and methods
 *     to modify it
 ***********************/
#ifndef STACK
#define STACK

class Stack
{
    public:
	Stack();
	~Stack();
	void push(card newData);
	card pop();
	bool isEmpty();
	card getTop();
	void printStack();
	friend ostream& operator <<(ostream& outs, const Stack& theStack);

    private:
	struct StackNode
	{
	    card data;
	    StackNode *next;
	};
	StackNode *top;
};
#endif
