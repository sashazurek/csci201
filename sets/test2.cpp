/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: *
 * Due Date: *
 * Last Modified: *
 ****************************************/

#include <iostream>
#include <fstream>
#include "set.h"

using namespace std;


int main() 
{
    string hold = "0011001100110011001100110011001100110011";
    Set temp;
    
    if (hold.length() == 40)
    {
        for (int i = 0; i < SETSIZE; i++)
        {
            if (hold[i] == '1')
                temp.add(i);
        }
    }

    cout << "Input string: " << hold << endl;
    cout << "Output Set  : " << temp << endl;
    
}
