#define SETSIZE 40
#include <iostream>

class Set
{
    private:
        int set[SETSIZE];       

    public:
        Set(); // constructor
        Set(const Set &oldset); // copy constructor
        int add(int element); // add element to set
        int remove(int element); // removes element from set
        friend Set operator+ (const Set &lhs, const Set &rhs); // Add two sets
        friend Set operator- (const Set &lhs, const Set &rhs); // Remove set from set
        friend Set operator* (const Set &lhs, const Set &rhs); // Intersect two sets
        friend Set operator~ (const Set &rhs); // Invert set
        friend int operator== (const Set &lhs, const Set &rhs);
        friend int operator!= (const Set &lhs, const Set &rhs);
        friend std::ostream& operator<<(std::ostream& outs, const Set &set);

};