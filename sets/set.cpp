#include "set.h"
/***********************************************************
 * Function: Set()
 * Author: Michael Zurek 
 * Purpose: To initialize a set
 * Parameters: None
 * Return Value: None
 * Last Modified: October 26th, 15:30
 **********************************************************/
Set::Set()
{
    int i;

    for (i = 0; i < SETSIZE; i++)
        this -> set[i] = 0;
}
/***********************************************************
 * Function: Set()
 * Author: Michael Zurek 
 * Purpose: To copy a set
 * Parameters: Set
 * Return Value: None
 * Last Modified: October 26th, 15:30
 **********************************************************/
Set::Set(const Set &oldset)
{
    int i;

    for (i = 0; i < SETSIZE; i++)
        this -> set[i] = oldset.set[i];
}
/***********************************************************
 * Function: add()
 * Author: Michael Zurek 
 * Purpose: To add an element to the set
 * Parameters: int
 * Return Value: int for fail/success
 * Last Modified: October 26th, 15:30
 **********************************************************/
int Set::add(int element)
{
    if (element >= 0 && element < SETSIZE)
    {
        this -> set[element] = 1;
        return 0; // success case
    }
    return 1; // fail case
}
/***********************************************************
 * Function: remove()
 * Author: Michael Zurek 
 * Purpose: To remove an element from the set
 * Parameters: int
 * Return Value: int for fail/success
 * Last Modified: October 26th, 15:30
 **********************************************************/
int Set::remove(int element)
{
    if (element >= 0 && element < SETSIZE)
    {
        this -> set[element] = 0;
        return 0; // success case
    }
    return 1; // fail case
}
/***********************************************************
 * Function: << Overload
 * Author: Michael Zurek 
 * Purpose: To print a set to the screen
 * Parameters: Ostream, set
 * Return Value: Ostream
 * Last Modified: October 26th, 15:30
 **********************************************************/
std::ostream& operator<<(std::ostream& outs, const Set &set)
{
    int i;
    for (i = 0; i < SETSIZE; i++)
    {
        outs << set.set[i];
    }
    return outs;
}
/***********************************************************
 * Function: + Overload
 * Author: Michael Zurek 
 * Purpose: To union two sets
 * Parameters: set, set
 * Return Value: set
 * Last Modified: October 26th, 15:40
 **********************************************************/
Set operator+ (const Set &lhs, const Set &rhs)
{
    int i;
    Set newSet;

    for (i = 0; i < SETSIZE; i++)
    {
        if (lhs.set[i] == 1 || rhs.set[i] == 1)
            newSet.set[i] = 1;
    }

    return newSet;
}
/***********************************************************
 * Function: - Overload
 * Author: Michael Zurek 
 * Purpose: To subtract
 * Parameters: set, set
 * Return Value: set
 * Last Modified: October 26th, 15:56
 **********************************************************/
Set operator- (const Set &lhs, const Set &rhs)
{
    int i;
    Set newSet;

    newSet = lhs;
    for (i = 0; i < SETSIZE; i++)
    {
        if (rhs.set[i] == 1 && lhs.set[i] == 1)
            newSet.set[i] = 0;
    }

    return newSet;
}
/***********************************************************
 * Function: * Overload
 * Author: Michael Zurek 
 * Purpose: To intersect two sets
 * Parameters: set, set
 * Return Value: set
 * Last Modified: October 26th, 16:02
 **********************************************************/
Set operator* (const Set &lhs, const Set &rhs)
{
    int i;
    Set newSet;

    for (i = 0; i < SETSIZE; i++)
    {
        if (rhs.set[i] == 1 && lhs.set[i] == 1)
            newSet.set[i] = 1;
    }

    return newSet;
}
/***********************************************************
 * Function: ~ Overload
 * Author: Michael Zurek 
 * Purpose: To negate a set
 * Parameters: set
 * Return Value: set
 * Last Modified: October 26th, 16:06
 **********************************************************/
Set operator~ (const Set &rhs)
{
    int i;
    Set newSet = rhs;

    for (i = 0; i < SETSIZE; i++)
    {
        if (newSet.set[i] == 1)
            newSet.set[i] = 0;
        else
            newSet.set[i] = 1;
    }

    return newSet;
}
/***********************************************************
 * Function: == Overload
 * Author: Michael Zurek 
 * Purpose: To determine if two sets are equal
 * Parameters: set, set
 * Return Value: int
 * Last Modified: October 26th, 16:25
 **********************************************************/
int operator== (const Set &lhs,const Set &rhs)
{
    int i = 0;
    
    while (lhs.set[i] == rhs.set[i] && i < SETSIZE)
        i++;

    if (i == SETSIZE)
        return 1;
    return 0;
}
/***********************************************************
 * Function: == Overload
 * Author: Michael Zurek 
 * Purpose: To determine if two sets are equal
 * Parameters: set, set
 * Return Value: int
 * Last Modified: October 26th, 16:28
 **********************************************************/
int operator!= (const Set &lhs,const Set &rhs)
{
    return !(lhs == rhs);
}