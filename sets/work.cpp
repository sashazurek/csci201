/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: *
 * Due Date: *
 * Last Modified: *
 ****************************************/

#define MAXBLUEPRINTS 100
#define MAXBUNS 1000000
#define EVENTS 1000
#include <iostream>
#include <fstream>
#include <string>
#include "set.h"
#include "time.h"


using namespace std;

struct blueprint
{
    string name;
    Set blueprint;
};

int main() 
{
    blueprint blueprints[MAXBLUEPRINTS];
    string parts[SETSIZE];
    string hold;
    ifstream blueprintfile("/colossus/classes/cs201/sets/blueprints");
    ifstream partfile("/colossus/classes/cs201/sets/parts");
    Set partsOnHand;
    int blueprint,i,partnum,bun;
    srandom(time(NULL));
    
    // intialization steps
    // save blueprints into memory
        // file is in format:
            // name
            // set
    blueprint = 0;
    while (getline(blueprintfile,hold))
    {
        // if input is a set
            // set the nth set in blueprints[]
        if (hold[0] == '0' || hold[0] == '1')
        {   
            for (i = 0; i < SETSIZE; i++)
            {
                if (hold[i] == '1')
                    blueprints[blueprint].blueprint.add(i);
            }
            // if set is modified, then that blueprint is done
            blueprint++;
        }
        // otherwise, it's a name
            // push word into name slot, add a space
        else
        {
            blueprints[blueprint].name  = hold;
        }
    }

    blueprintfile.close();

    // Open parts names and save them into an array
    i = 0;
    
    while (getline(partfile,hold))
    {
        parts[i] = hold;
        i++;
    }
    partfile.close();

    // events loop
    for (i = 0; i < EVENTS; i++)
    {
        // generate found part and add to set
        partnum = random()%SETSIZE;
        partsOnHand.add(partnum);
        // generate bun that found part
        bun = random()%MAXBUNS;
        // report part and bun
        cout << "Bun " << bun << " has found part " << parts[partnum] << "!" << endl;
        // check blueprints to see if one can be built
        // for each blueprint
        blueprint = 0;
        while (blueprints[blueprint].name != "")
        {
            // if intersect of blueprint[i] and partsOnHand is equal to blueprint[i]
            if (blueprints[blueprint].blueprint * partsOnHand ==
                 blueprints[blueprint].blueprint)
            {
                // remove blueprint[i] from partsOnHand
                partsOnHand = partsOnHand - blueprints[blueprint].blueprint;
                // report that item in blueprint[i] has been built
                cout << endl << blueprints[blueprint].name << " has been built!\n\n";
            }
            blueprint++;
        }
    }    
    
    return 0;
}
