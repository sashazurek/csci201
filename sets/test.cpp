/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: *
 * Due Date: *
 * Last Modified: *
 ****************************************/

#include <iostream>
#include "set.h"

using namespace std;


int main() 
{
    Set original, test, result;

    cout << "Testing Add and Copy\n";
    cout << "Adding every other element, from 0th\n";
    for (int i = 0; i < SETSIZE; i += 2)
        original.add(i);
    
    test = original;

    cout << "Original: " << original << endl;
    cout << "Test: " << test << endl << endl;

    cout << "Testing remove\n";
    cout << "Removing every 4th element from test, from 0th\n";

    for (int i = 0; i < SETSIZE; i += 4)
        test.remove(i);
    
    cout << "test: "<< test << endl << endl;

    cout << "Testing union\n";
    cout << "Adding original (every other element from 0th) and test\n"
        << "(every other element from 1st) should result in full set\n";

    test = Set();
    for (int i = 1; i < SETSIZE; i += 2)
        test.add(i);
    
    result = original + test;

    cout << "Original: " << original << endl;
    cout << "Test: " << test << endl;
    cout << "Result: " << result << endl << endl;

    cout << "Testing subtract\n";
    
    for (int i = 0; i < SETSIZE; i++)
        original.add(i);
    cout << "Original: " << original << endl;

    test = Set();
    for (int i = 0; i < SETSIZE; i += 3)
        test.add(i);
    cout << "Test: " << test << endl;

    result = original - test;
    cout << "Result: " << result << endl << endl;
    
    cout << "Testing intersect\n";
    cout << "Original: " << original << endl;

    test = Set();
    for (int i = 0; i < SETSIZE; i += 9)
        test.add(i);
    cout << "Test: " << test << endl;

    result = original * test;
    cout << "Result: " << result << endl << endl;

    cout << "Testing inversion\n";

    test = Set();
    cout << "Test: " << test << endl;

    result = ~test;
    cout << "Result: " << result << endl << endl;
    
    cout << "Testing equality\n";
    cout << "Original: " << original << endl;

    test = Set();
    cout << "Test: " << test << endl;

    cout << "Equal? ";
    if (original == test)
        cout << "Yes\n";
    else
        cout << "No\n";

    original.remove(20);
    cout << "Original: " << original << endl;

    test = original;

    cout << "Test: " << test << endl;

    cout << "Equal? ";
    if (original == test)
        cout << "Yes\n";
    else
        cout << "No\n";
   
    cout << "Testing inequality\n";
    original = ~Set();
    cout << "Original: " << original << endl;

    test = Set();
    cout << "Test: " << test << endl;

    cout << "Not Equal? ";
    if (original != test)
        cout << "Yes\n";
    else
        cout << "No\n";
    return 0;
}
