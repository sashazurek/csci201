/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: *
 * Due Date: *
 * Last Modified: *
 ****************************************/

#define EVENTS 10
#define MAXBUNS 1
#include <iostream>
#include <fstream>
#include "bintree.h"

using namespace std;


int main() 
{
    Bintree bunnies;
    node * temp;
    Bunny tempBun;
    ifstream infile;
    ofstream ofile;
    int event;
    int killType;

    bunnies.loadBuns(infile);
    
    tempBun = Bunny(1453);
    tempBun.tickCauliflower();
    tempBun.tickComment();
    tempBun.tickComment();
    tempBun.tickSock();

    bunnies.insert(tempBun);

    ofile.open("bunny.data",ofstream::out | ofstream::trunc);
    if (!ofile)
        ofile << "\n";
    bunnies.saveBuns(ofile);
    ofile.close();


    return 0;
}
