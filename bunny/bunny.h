class Bunny
{
    private:
        int number;
        int oldExp;
        int newExp;
        int sockKills;
        int flyKills;
        int commentKills;
        int cauliflowerKills;
        // Each tick function will increase bunny experience automatically
        void increaseExperience(int points);
    public:
        // Default Constructor
        Bunny();
        // Primary constructor. Bunnies are delcared with their num.
        Bunny(int newNum);
        // Load constructor, used for loading bun data
        Bunny(int num, int exp, int sock, int fly, int comment, int cauliflower);
        // Tick functions increase the num of kills per class by 1
        void tickSock();
        void tickFly();
        void tickComment();
        void tickCauliflower();
        // Get functions grab number of kills for comparison to other buns
        int getSockKills();
        int getFlyKills();
        int getCommentKills();
        int getCauliflowerKills();
        // getLevel converts experience amount to the appropriate level
        int getLevel();
        // getNum simply gets num -- buns are initialized before placed in tree
        int getNum();
        // Grabs experience 
        int getExp();
        // Update adds a bun's kills and exp to another bun -- duplicate case in bintree
        void update(Bunny bun);
        // Reset zeroes out a bunny's values -- for use in main()
        void reset();
};