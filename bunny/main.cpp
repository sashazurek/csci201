/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: *
 * Due Date: *
 * Last Modified: *
 ****************************************/
#define EVENTS 1000000
#define MAXBUNS 1000000
#include <iostream>
#include <fstream>
#include "bintree.h"
#include "stdlib.h"
#include "time.h"

using namespace std;
/***********************************************************
 * Function: record_display()
 * Author: Michael Zurek 
 * Purpose: To display daily records
 * Parameters: Record of type stats
 * Return Value: None
 * Last Modified: October 17th, 16:04
 **********************************************************/
void record_display(stats &record)
{
    // report best kills stats
        // most sock kills
        // most fly kills
        // most uncommented code kills
        // most cauliflower kills
    // report num of buns in each level (excluding level 0)
    printf("Bun %i had the most sock kills!\n",record.bestSock);
    printf("Bun %i had the most fly kills!\n",record.bestFly);
    printf("Bun %i had the most uncommented code kills!\n",record.bestComment);
    printf("Bun %i had the most cauliflower kills!\n",record.bestCauliflower);
    printf("%i buns were level 0!\n",record.levelZeroBuns);
    printf("%i buns were level 1!\n",record.level1Buns);
    printf("%i buns were level 2!\n",record.level2Buns);
    printf("%i buns were level 3!\n",record.level3Buns);
    printf("%i buns were level 4!\n",record.level4Buns);
    printf("%i buns were level 5!\n",record.level5Buns);
    printf("%i buns were level 6!\n",record.level6Buns);
    printf("%i buns were level 7!\n",record.level7Buns);
    printf("%i buns were level 8!\n",record.level8Buns);
    printf("%i buns were level 9!\n",record.level9Buns);
    printf("%i buns were level 10!\n",record.level10Buns);
    printf("%i buns were level 11!\n",record.level11Buns);
    printf("%i buns were level 12!\n",record.level12Buns);
    printf("%i buns were level 13!\n",record.level13Buns);
    printf("%i buns were level 14!\n",record.level14Buns);
    printf("%i buns were level 15!\n",record.level15Buns);
}
/***********************************************************
 * function: record_init()
 * author: michael zurek 
 * purpose: Initialize data of type stats
 * parameters: Record of type stats
 * return value: none
 * last modified: october 17th, 16:04
 **********************************************************/
void record_init(stats &record)
{
    record.bestSock = -1;
    record.bestFly = -1;
    record.bestComment = -1;
    record.bestCauliflower = -1;
    record.levelZeroBuns = 0;
    record.level1Buns = 0;
    record.level2Buns = 0;
    record.level3Buns = 0;
    record.level4Buns = 0;
    record.level5Buns = 0;
    record.level6Buns = 0;
    record.level7Buns = 0;
    record.level8Buns = 0;
    record.level9Buns = 0;
    record.level10Buns = 0;
    record.level11Buns = 0;
    record.level12Buns = 0;
    record.level13Buns = 0;
    record.level14Buns = 0;
    record.level15Buns = 0;
}

int main() 
{
    Bintree bunnies;
    Bunny tempBun;
    node * insertStorage;
    stats record;
    int event;
    int killType;
    ifstream infile;
    ofstream ofile;

    // initialize random generator and node for storage
    srandom(time(NULL));
    insertStorage = new node;

    // load in bunny data if it exists
    infile.open("bunny.data");
    if (infile.fail())
        cout << "Unable to open bunny.data - running w/ new data\n";
    else
    {
        bunnies.loadBuns(infile);
    }
    infile.close();
    

    // event generator (production is 1,000,000 events a day)
    for (event = 0; event <= EVENTS; event++)
    {
        // find num of bun that did a thing
            // assign this num to the storage bun
        tempBun = Bunny(random()%MAXBUNS);
        // find num of thing bun did
        killType = random()%4 + 1;
            // assign this num to the storage bun
            switch (killType)
            {
                case 1:
                    tempBun.tickSock();
                    break;
                case 2:
                    tempBun.tickFly();
                    break;
                case 3:
                    tempBun.tickComment();
                    break;
                case 4:
                    tempBun.tickCauliflower();
                    break;
            }
        // send storage bun to binary tree
        insertStorage = bunnies.insert(tempBun);
        // reset tempbun to prevent crosspollination
        tempBun.reset();
        
    }
    // find statistics for bunny bureau and report them
    record_init(record);
    bunnies.findBunData(record, MAXBUNS);
    record_display(record);
    
    // save bun data to file
    ofile.open("bunny.data",ofstream::out | ofstream::trunc);
    if (!ofile)
        ofile << "\n";
    bunnies.saveBuns(ofile);
    ofile.close();

    return 0;
}
