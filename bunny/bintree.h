#include "bunny.h"
#include <fstream>

struct node
{
    Bunny nodeBun;

    node * left;
    node * right;

};

// Data pack of all stats needed to report to bunny bureau
struct stats
{
    int bestSock;
    int bestFly;
    int bestComment;
    int bestCauliflower;

    int levelZeroBuns;
    int level1Buns;
    int level2Buns;
    int level3Buns;
    int level4Buns;
    int level5Buns;
    int level6Buns;
    int level7Buns;
    int level8Buns;
    int level9Buns;
    int level10Buns;
    int level11Buns;
    int level12Buns;
    int level13Buns;
    int level14Buns;
    int level15Buns;
};

class Bintree
{
    private:
        node * root;
        node * insert_helper(node * curr, Bunny bun);
        node * find_helper(node * curr, int bun);
        // Both save and trav helper use pre-order.
        // As the actions are different, the functions are too
        void save_helper(node * curr, std::ofstream &ofile);
        void findBunData_helper(node * curr, stats &report);

    public:
        Bintree();
        node * insert(Bunny bun);
        node * find(int bun);
        void saveBuns(std::ofstream &ofile);
        void loadBuns(std::ifstream &infile);
        void findBunData(stats &report, int maxbuns); 
};