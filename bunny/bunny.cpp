#include "bunny.h"
#include <iostream>
/***********************************************************
 * Function: Bunny()
 * Author: Michael Zurek 
 * Purpose: Default constructor
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
Bunny::Bunny()
{
    number = -1;
    oldExp = 0;
    newExp = 0;
    sockKills = 0;
    flyKills = 0;
    commentKills = 0;
    cauliflowerKills = 0;
}/***********************************************************
 * Function: Bunny(int newNum)
 * Author: Michael Zurek 
 * Purpose: Constructor with given num
 * Parameters: newNum, int
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
Bunny::Bunny(int newNum)
{
    number = newNum;
    oldExp = 0;
    newExp = 0;
    sockKills = 0;
    flyKills = 0;
    commentKills = 0;
    cauliflowerKills = 0;
}
Bunny::Bunny(int num, int exp, int sock, int fly, int comment, int cauliflower)
{
    number = num;
    oldExp = exp;
    newExp = exp;
    sockKills = sock;
    flyKills = fly;
    commentKills = comment;
    cauliflowerKills = cauliflower;
}

/***********************************************************
 * Function: increaseExperience()
 * Author: Michael Zurek 
 * Purpose: To increase bun exp with given points
 *      and track levelup
 * Parameters: Points, type int
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::increaseExperience(int points)
{
    newExp += points;
}
/***********************************************************
 * Function: tickSock()
 * Author: Michael Zurek 
 * Purpose: To increment sock kills
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::tickSock()
{
    sockKills++;
    increaseExperience(1);
}
/***********************************************************
 * Function: tickFly()
 * Author: Michael Zurek 
 * Purpose: To increment Fly kills
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::tickFly()
{
    flyKills++;
    increaseExperience(3);
}
/***********************************************************
 * Function: tickComment()
 * Author: Michael Zurek 
 * Purpose: To increment comment kills
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::tickComment()
{
    commentKills++;
    increaseExperience(7);
}
/***********************************************************
 * Function: tickCauliflower()
 * Author: Michael Zurek 
 * Purpose: To increment cauliflower kills
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::tickCauliflower()
{
    cauliflowerKills++;
    increaseExperience(11);
}
/***********************************************************
 * Function: getSockKills()
 * Author: Michael Zurek 
 * Purpose: To get sock kills
 * Parameters: Command
 * Return Value: Int for sock kills 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getSockKills()
{
    return sockKills;
}
/***********************************************************
 * Function: getFlyKills()
 * Author: Michael Zurek 
 * Purpose: To get fly kills of bun
 * Parameters: None
 * Return Value: Int for fly kills 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getFlyKills()
{
    return flyKills;
}
/***********************************************************
 * Function: getCommentKills()
 * Author: Michael Zurek 
 * Purpose: To get comment kills
 * Parameters: None
 * Return Value: Int for comment kills 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getCommentKills()
{
    return commentKills;
}
/***********************************************************
 * Function: getCauliflowerKills()
 * Author: Michael Zurek 
 * Purpose: To get number of cauliflower kills
 * Parameters: None
 * Return Value: Int for cauliflower kills 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getCauliflowerKills()
{
    return cauliflowerKills;
}
/***********************************************************
 * Function: getLevel()
 * Author: Michael Zurek 
 * Purpose: To find bun level based on exp
 * Parameters: None
 * Return Value: Int for level 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getLevel()
{
    if (newExp >= 1200)
        return 15;
    else if (newExp >= 1050)
        return 14;
    else if (newExp >= 910)
        return 13;
    else if (newExp >= 780)
        return 12;
    else if (newExp >= 660)
        return 11;
    else if (newExp >= 550)
        return 10;
    else if (newExp >= 450)
        return 9;
    else if (newExp >= 360)
        return 8;
    else if (newExp >= 280)
        return 7;
    else if (newExp >= 210)
        return 6;
    else if (newExp >= 150)
        return 5;
    else if (newExp >= 100)
        return 4;
    else if (newExp >= 60)
        return 3;
    else if (newExp >= 30)
        return 2;
    else if (newExp >= 10)
        return 1;

    return 0;
}
/***********************************************************
 * Function: getNum()
 * Author: Michael Zurek 
 * Purpose: To grab key number of bun
 * Parameters: None
 * Return Value: Int for bun number 
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny::getNum()
{
    return number;
}
/***********************************************************
 * Function: getExp()
 * Author: Michael Zurek 
 * Purpose: **DEBUG** To grab experience from bun
 * Parameters: None
 * Return Value: Int for experience
 * Last Modified: October 8th, 16:04
 **********************************************************/
int Bunny:: getExp()
{
    return newExp;
}
/***********************************************************
 * Function: update()
 * Author: Michael Zurek 
 * Purpose: To update a bun with new info 
 * Parameters: A bunny (which is guaranteed to have same
 *  ID as the bun it is updating)
 * Return Value: None
 * Last Modified: October 8th, 16:04
 **********************************************************/
void Bunny::update(Bunny bun)
{

    sockKills = sockKills + bun.sockKills;
    flyKills = flyKills + bun.flyKills;
    commentKills = commentKills + bun.commentKills;
    cauliflowerKills = cauliflowerKills + bun.cauliflowerKills;
    this -> newExp += bun.newExp;
    this -> oldExp += bun.oldExp;
    if (oldExp < 10 && newExp >= 10)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 30 && newExp >= 30)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 60 && newExp >= 60)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 100 && newExp >= 100)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 150 && newExp >= 150)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 210 && newExp >= 210)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 280 && newExp >= 280)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 360 && newExp >= 360)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 450 && newExp >= 450)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 550 && newExp >= 550)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 660 && newExp >= 660)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 780 && newExp >= 780)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 910 && newExp >= 910)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 1050 && newExp >= 1050)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else if (oldExp < 1200 && newExp >= 1200)
    {
        oldExp = newExp;
        printf("Bun %i is now level %i\n",this -> number, this -> getLevel());
    }
    else
    {
        oldExp = newExp;
    }
}
/***********************************************************
 * Function: reset()
 * Author: Michael Zurek 
 * Purpose: To nuke a bun's values
 * Parameters: None
 * Return Value: None
 * Last Modified: October 14th, 16:04
 **********************************************************/
void Bunny::reset()
{
    number = -1;
    oldExp = 0;
    newExp = 0;
    sockKills = 0;
    flyKills = 0;
    commentKills = 0;
    cauliflowerKills = 0;
}