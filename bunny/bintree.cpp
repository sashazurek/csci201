#include "bintree.h"
#include <iostream>
#include <fstream>

using namespace std;
/***********************************************************
 * Function: Bintree()
 * Author: Michael Zurek 
 * Purpose: Default Constructor
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:27
 **********************************************************/
Bintree::Bintree()
{
    root = NULL;
}
/***********************************************************
 * Function: insert()
 * Author: Michael Zurek 
 * Purpose: To push a bunny to the binary tree
 * Parameters: Bun of type Bunny
 * Return Value: Node * of inserted bun
 * Last Modified: October 8th, 16:27
 **********************************************************/
node * Bintree::insert(Bunny bun)
{
    if (root == NULL)
    {
        root = new node;
        root -> nodeBun = bun;
        root -> left = NULL;
        root -> right = NULL;
        return root;
    }
    return insert_helper(root,bun);
}
/***********************************************************
 * Function: insert_helper()
 * Author: Michael Zurek 
 * Purpose: To find the insert point for a new bun
 *     ** RECURSIVE**
 * Parameters: Pointer of current node and bun
 * Return Value: Pointer of node
 * Last Modified: October 8th, 16:27
 **********************************************************/
node * Bintree::insert_helper(node * curr, Bunny bun)
{
    if (curr -> nodeBun.getNum() == bun.getNum())
    {
        curr -> nodeBun.update(bun);
        return curr;
    }
    if (bun.getNum() < curr -> nodeBun.getNum())
    {
        if (curr -> left == NULL)
        {
            curr -> left = new node; // probably fine, wait until compile
            curr -> left -> nodeBun = bun;
            curr -> left -> left = NULL;
            curr -> left -> right = NULL;
            return curr -> left;
        }
        return insert_helper(curr -> left, bun);
    }
    if (bun.getNum() > curr -> nodeBun.getNum())
    {
        if (curr -> right == NULL)
        {
            curr -> right = new node; // probably fine, wait until compile
            curr -> right -> nodeBun = bun;
            curr -> right -> left = NULL;
            curr -> right -> right = NULL;
            return curr -> right;
        }
    }
    return insert_helper(curr -> right, bun);
}
/***********************************************************
 * Function: find()
 * Author: Michael Zurek 
 * Purpose: To find node given a bun ID
 * Parameters: Int for bun ID
 * Return Value: Node pointer for found bun
 * Last Modified: October 8th, 16:27
 **********************************************************/
node * Bintree::find(int bun)
{
    return find_helper(root,bun);
}
/***********************************************************
 * Function: find_helper()
 * Author: Michael Zurek 
 * Purpose: To recursively find the node pointer that points
 *      to bun of ID bun
 * Parameters: Node pointer, int for bun id
 * Return Value: Node pointer
 * Last Modified: October 8th, 16:27
 **********************************************************/
node * Bintree::find_helper(node * curr, int bun)
{
    if (curr == NULL)
        return NULL;
    if (curr -> nodeBun.getNum() == bun)
        return curr;
    if (bun < curr -> nodeBun.getNum())
        return find_helper(curr -> left, bun);
    return find_helper(curr -> right,bun);
}

/***********************************************************
 * Function: saveBuns()
 * Author: Michael Zurek 
 * Purpose: To save a bintree to a file
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:27
 **********************************************************/
void Bintree::saveBuns(ofstream &ofile)
{
    save_helper(root, ofile);
}
/***********************************************************
 * Function: save_helper()
 * Author: Michael Zurek 
 * Purpose: To help save a bintree to a file
 * Parameters: Pointer to current node
 * Return Value: None
 * Last Modified: October 8th, 16:27
 **********************************************************/
void Bintree::save_helper(node * curr, ofstream &ofile)
{
    if (curr != NULL)
    {
        // getExp only grabs newExp. This is fine, as all buns should have
            // equivalent old and new exp at the point this function runs
        ofile << curr -> nodeBun.getNum() << " " << curr -> nodeBun.getExp() << " ";
        ofile << curr -> nodeBun.getSockKills() << " " << curr -> nodeBun.getFlyKills() << " ";
        ofile << curr -> nodeBun.getCommentKills() << " " << curr -> nodeBun.getCauliflowerKills();
        ofile << endl;
        save_helper(curr -> left,ofile);
        save_helper(curr -> right,ofile);
    }
}
/***********************************************************
 * Function: loadBuns()
 * Author: Michael Zurek 
 * Purpose: To load a bintree from a file
 * Parameters: None
 * Return Value: None
 * Last Modified: October 17th, 16:27
 **********************************************************/
void Bintree::loadBuns(ifstream &infile)
{
    int number;
    int exp;
    int sockKills;
    int flyKills;
    int commentKills;
    int cauliflowerKills;
    Bunny newBun;
    while (infile)
    {
        infile >> number;
        infile >> exp;
        infile >> sockKills;
        infile >> flyKills;
        infile >> commentKills;
        infile >> cauliflowerKills;

        newBun = Bunny(number,exp,sockKills,flyKills,commentKills,cauliflowerKills);

        insert(newBun);
    }
}
/***********************************************************
 * Function: findBunData()
 * Author: Michael Zurek 
 * Purpose: To find bun stats to report to Bureau
 * Parameters: None
 * Return Value: None
 * Last Modified: October 8th, 16:27
 **********************************************************/
void Bintree::findBunData(stats &report, int maxbuns)
{
    int nonZeroBuns;
    findBunData_helper(root, report);
    nonZeroBuns = report.level1Buns +
        report.level2Buns +
        report.level3Buns +
        report.level4Buns +
        report.level5Buns +
        report.level6Buns +
        report.level7Buns +
        report.level8Buns +
        report.level9Buns +
        report.level10Buns +
        report.level11Buns +
        report.level12Buns +
        report.level13Buns +
        report.level14Buns +
        report.level15Buns;
    report.levelZeroBuns = maxbuns - nonZeroBuns;
}
/***********************************************************
 * Function: findBunData_helper()
 * Author: Michael Zurek 
 * Purpose: To help find bun stats to report to Bureau
 * Parameters: Pointer to current node, reference to stats file
 * Return Value: None
 * Last Modified: October 8th, 16:27
 **********************************************************/
void Bintree::findBunData_helper(node * curr, stats &report)
{
    if (curr == NULL)
        return;

    // If record is freshly-initialized, current bun is the best :)
    // Only need to check bestSock since it can only be -1 when everything else is too
    if (report.bestSock == -1)
    {
        report.bestSock = curr -> nodeBun.getLevel();
        report.bestFly = curr -> nodeBun.getLevel();
        report.bestComment = curr -> nodeBun.getLevel();
        report.bestCauliflower = curr -> nodeBun.getLevel();
    }
    // Otherwise, check current best bun against curr's bun
        // and update if curr's bun is better
    else
    {
        if (curr -> nodeBun.getSockKills() > find(report.bestSock) -> nodeBun.getSockKills())
            report.bestSock = curr -> nodeBun.getNum();
        if (curr -> nodeBun.getFlyKills() > find(report.bestSock)-> nodeBun.getFlyKills())
            report.bestFly = curr -> nodeBun.getNum();
        if (curr -> nodeBun.getCommentKills() > find(report.bestSock) -> nodeBun.getCommentKills())
            report.bestComment = curr -> nodeBun.getNum();
        if (curr -> nodeBun.getCauliflowerKills() > find(report.bestSock) -> nodeBun.getCauliflowerKills())
            report.bestCauliflower = curr -> nodeBun.getNum();
    }


    switch (curr -> nodeBun.getLevel())
    {
        case 1:
            report.level1Buns++;
            break;
        case 2:
            report.level2Buns++;
            break;
        case 3:
            report.level3Buns++;
            break;
        case 4:
            report.level4Buns++;
            break;
        case 5:
            report.level5Buns++;
            break;
        case 6:
            report.level6Buns++;
            break;
        case 7:
            report.level7Buns++;
            break;
        case 8:
            report.level8Buns++;
            break;
        case 9:
            report.level9Buns++;
            break;
        case 10:
            report.level10Buns++;
            break;
        case 11:
            report.level11Buns++;
            break;
        case 12:
            report.level12Buns++;
            break;
        case 13:
            report.level13Buns++;
            break;
        case 14:
            report.level14Buns++;
            break;
        case 15:
            report.level15Buns++;
            break;
    }
    findBunData_helper(curr -> left, report);
    findBunData_helper(curr -> right, report);
}