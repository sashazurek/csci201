#include "canvas.h"
#include <string.h>
#include <iostream>

using namespace std;

Canvas::Canvas()
{
    int x;
    int y;
    for (y = 0; y < CANVASY; y++)
    {
        for (x = 0; x < CANVASX; x++)
            canvas[y][x] = ' ';
    } 
}

/***********************************************************
 * Function: clear()
 * Author: Michael Zurek 
 * Purpose: To clear the display
 * Parameters: Nothing
 * Return Value: Nothing 
 * Last Modified: September 13th, 18:14
 **********************************************************/
void Canvas::clear()
{
    int i;

    for (i = 0; i < 25; i++)
    {
        cout << endl;
    }
    return;
}
/***********************************************************
 * Function: draw()
 * Author: Michael Zurek 
 * Purpose: To put a certain character at a certain location
 * Parameters: Char for stroke, int x and y for coords,
 *  Command theCommand for a command
 * Return Value: 0 for success, 1 for unsuccess 
 * Last Modified: September 18th, 17:00
 **********************************************************/
void Canvas::draw(char brush, int x, int y, Command theCommand)
{
    if (x < CANVASX && y < CANVASY)
    {
        canvas[y][x] = brush;
    }
    else
        cout << "Invalid coordinate.\n";
    if (brush == ' ')
        return;
    if (undostack.push(theCommand) == 0)
        return;
    else 
    {
        cout << "Did not save command\n";
        return;
    }
}
/***********************************************************
 * Function: undo()
 * Author: Michael Zurek 
 * Purpose: To undo a previously done command
 * Parameters: Command input
 * Return Value: Nothing 
 * Last Modified: September 17th, 20:26
 **********************************************************/
void Canvas::undo(Command &input)
{
    Command temp;
    int finder = 0;
    int i;
    if (undostack.pop(temp) == 0)
    {
        strncpy(input.cmd,temp.cmd,COMSIZE);
        redostack.push(temp);
        while (input.cmd[finder] != '"')
            finder++;
        input.cmd[finder+1] = ' ';
    }
    else
        cout << "No command to undo\n";
    
}
/***********************************************************
 * Function: redo()
 * Author: Michael Zurek 
 * Purpose: To redo an undone action
 * Parameters: Command
 * Return Value: Nothing 
 * Last Modified: September 18th, 17:01
 **********************************************************/
void Canvas::redo(Command &theCommand)
{
    Command temp;
    int finder = 0;
    if (redostack.pop(temp) == 0)
    {
        strncpy(theCommand.cmd,temp.cmd,COMSIZE);
        undostack.push(temp);
    }
    else
        cout << "No command to redo\n";
}
/***********************************************************
 * Function: printCanvas()
 * Author: Michael Zurek 
 * Purpose: To print the canvas
 * Parameters: Nothing
 * Return Value: Nothing 
 * Last Modified: September 16th, 20:26
 **********************************************************/
void Canvas::printCanvas()
{
    int x,y;

    for (y = 0; y < CANVASY; y++)
    {
        for (x = 0; x < CANVASX; x++)
            cout << canvas[y][x];
        cout << endl;
    }

}
/***********************************************************
 * Function: printLine()
 * Author: Michael Zurek 
 * Purpose: To add a dividing line between canvas prints
 * Parameters: Nothing
 * Return Value: Nothing 
 * Last Modified: September 16th, 20:26
 **********************************************************/
void Canvas::printLine()
{
    int i;

    for (i = 0; i < 70; i++)
        cout << "-";
    cout << endl;
}
/***********************************************************
 * Function: quit()
 * Author: Michael Zurek 
 * Purpose: To exit the app
 * Parameters: Nothing
 * Return Value: -1, which indicates exit
 * Last Modified: September 16th, 20:26
 **********************************************************/
int Canvas::quit()
{
    return -1;
}