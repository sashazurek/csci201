#define CANVASX 40
#define CANVASY 20
#include "arrstack.h"

/***********************
 * Class Name: Canvas
 * Purpose: Create a painting canvas that handles
 *      drawing and saves commands for undo and redo 
 ***********************/
class Canvas
{
    private:
        char canvas[CANVASY][CANVASX];
        ArrStack undostack;
        ArrStack redostack;
    public:
        Canvas();
        void draw(char stroke,int x, int y,Command theCommand);
        void clear();
        void printCanvas();
        void printLine();
        void undo(Command &theCommand); 
        void redo(Command &theCommand);
        int quit();
};