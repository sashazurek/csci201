#include "arrstack.h"
#include <string.h>
using namespace std;


ArrStack::ArrStack()
{
    ptr = 0;
}
/***********************************************************
 * Function: push()
 * Author: Michael Zurek 
 * Purpose: To push a Command to the ArrStack
 * Parameters: Command
 * Return Value: Int for status flag 
 * Last Modified: September 18th, 17:04
 **********************************************************/
int ArrStack::push(Command theCommand)
{
    Command temp;

    if (ptr == SIZE)
        return -1;
    strncpy(temp.cmd,theCommand.cmd,COMSIZE);
    stack[ptr] = temp;
    ptr++;
    return 0;
}
/***********************************************************
 * Function: pop()
 * Author: Michael Zurek 
 * Purpose: To pop a Command from the ArrStack
 * Parameters: Command
 * Return Value: Int for status flag 
 * Last Modified: September 18th, 17:04
 **********************************************************/
int ArrStack::pop(Command &theCommand)
{
    if (isEmpty())
        return -1; 
    
    ptr--;
    strncpy(theCommand.cmd,stack[ptr].cmd,COMSIZE);
    
    return 0;
}
/***********************************************************
 * Function: isEmpty()
 * Author: Michael Zurek 
 * Purpose: To check if the ArrStack is empty
 * Parameters: None
 * Return Value: Int for status flag 
 * Last Modified: September 18th, 17:04
 **********************************************************/
int ArrStack::isEmpty()
{
    return ptr == 0;
}
/***********************************************************
 * Function: peek()
 * Author: Michael Zurek 
 * Purpose: To take a look at the top of the ArrStack
 * Parameters: Command
 * Return Value: Int for status flag 
 * Last Modified: September 18th, 17:04
 **********************************************************/
int ArrStack::peek(Command &theCommand)
{
    if (isEmpty())
        return -1;
    
    strncpy(theCommand.cmd,stack[ptr-1].cmd,COMSIZE);

    return 0;
}