/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 201, Fall 2019           *
 * Assignment: ASCII Paint              *
 * Due Date: September 19th 2019        *
 * Last Modified: *
 ****************************************/

#include <iostream>
#include "canvas.h"
#include "string.h"
#include "stdlib.h"

using namespace std;
/***********************************************************
 * Function: inputToCommand()
 * Author: Michael Zurek 
 * Purpose: To parse inputs into function calls of Canvas
 * Parameters: Command input and the Canvas class
 * Return Value: Int for status flags 
 * Last Modified: September 18th, 17:04
 **********************************************************/
int inputToCommand(Command input,Canvas & theCanvas)
{
/***********************************************************
 * Command specifications:
 * Allowed Commands:
 *      draw " " at row,column
 *          Draws input char at a coordinate
 *      undo
 *          Undoes previous command
 *      redo
 *          Redoes previously undone command
 *      quit
 *          Exits the app
 **********************************************************/
    int x,y,hold;
    int temp = 0;
    int lengthOfNum = 0;
    char brush;
    NumStr coord;

    // If input is not a draw command:
        // Quit will quit
        // Undo will pull a draw command off the stack
            // and replace the char with a space
        // Redo will pull a draw command off its stack and send it
    if (strncmp(input.cmd,"quit",4) == 0)
        return -1;
    else if (strncmp(input.cmd,"undo",4) == 0)
    {
        theCanvas.undo(input);
    }
    else if (strncmp(input.cmd,"redo",4) == 0)
    {
        theCanvas.redo(input);
    }
    if (strncmp(input.cmd,"draw",4) == 0)
    {
        // Find brushstroke and save it
        // Brushstroke is first char after first "
        while (input.cmd[temp] != '"')
            temp++;
        brush = input.cmd[temp+1];
        // Find x coord and save it
        // X coord will be the first number encountered after pos 7
        temp = 7;
        while (input.cmd[temp] > 57 || input.cmd[temp] < 48) 
            temp++;
        // Check if next char is also a number
            // If not, simply set temp to x
            // If so, allocate double digit and set x to the full num
        if (input.cmd[temp+1] == ',')
        {
            // Converting char to int uses '0' as the modifier
            x = input.cmd[temp] - '0';
            // Increase temp to pos of the coord marker
            // In this case, it's just +1
            temp++;
        }
        else
        {
            hold = temp;
            while (input.cmd[temp] != ',')
            {
                temp++;
                lengthOfNum++;
            }
            // Assign num to the coord struct for sending to atoi
            coord.num[0] = input.cmd[hold];
            // Final num is hold, plus number length, minus one
            coord.num[1] = input.cmd[hold+lengthOfNum-1];
            // Use atoi() to convert cstring to int
            x = atoi(coord.num);
        }
        // Find y coord and save it
        // Y coord will be first number encountered after pos of last num            
        while (input.cmd[temp] > 57 || input.cmd[temp] < 48) 
            temp++;
        // Check if next char is also a number
            // If not, simply set temp to y
            // If so, allocate double digit and set y to the full num
        if (input.cmd[temp+1] > 57 || input.cmd[temp+1] < 48)
        {
            // Converting char to int uses '0' as the modifier
            y = input.cmd[temp] - '0';
            // Temp does not need to be increased here, because we're done
        }
        // Else case: Not implemented yet.
        else
        {
            hold = temp;
            while (input.cmd[temp] != ',')
            {
                temp++;
                lengthOfNum++;
            }
            // Assign num to the coord struct for sending to atoi
            coord.num[0] = input.cmd[hold];
            // Final num is hold, plus number length, minus one
            coord.num[1] = input.cmd[hold+lengthOfNum-1];
            // Use atoi() to convert cstring to int
            y = atoi(coord.num);
        }       // Code will be nearly identical to code for x coord

        // Finally, put in the requested stroke.
        theCanvas.draw(brush,x,y,input);

    }
        
    return 0;
}

int main() 
{
    Canvas theCanvas;
    Command input;
    int status = 0;

    do
    {
        theCanvas.clear();
        theCanvas.printLine();
        theCanvas.printCanvas();
        theCanvas.printLine();
        cin.getline(input.cmd,sizeof(input.cmd)); 
        status = inputToCommand(input,theCanvas);
    } while (status == 0);

    return 0;
}
