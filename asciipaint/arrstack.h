#define SIZE 100
#define COMSIZE 20
#define DIMENSIONPLACES 2

/***********************************************************
 * Command specifications:
 * Allowed Commands:
 *      draw " " at row,column
 *          Draws input char at a coordinate
 *      undo
 *          Undoes previous command
 *      redo
 *          Redoes previously undone command
 *      quit
 *          Exits the app
 **********************************************************/
struct Command
{
    char cmd[COMSIZE];
};
// Used for converting multiplace coordinates into ints
struct NumStr
{
    char num[DIMENSIONPLACES];
};
/***********************
 * Class Name: ArrStack
 * Purpose: Establish a stack data structure and methods
 *     to modify it
 ***********************/
class ArrStack
{
    private:
        Command stack[SIZE];
        int ptr;

    public:
        
        ArrStack();
        int push(Command theCommand);
        int isEmpty();
        int peek(Command &theCommand);
        int pop(Command &theCommand);
};